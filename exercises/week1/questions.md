# Questions week 1 


## Calling pdb 
`pdb hello-exo2.py 50` does not work 


# Python pdb 
Q: Is it possible to see variables without 

```
import pdb 
pdb.set_trace()
```

## Try 1 without import pdb and set_trace()

Q: unable to see the variables 
`python3 -m pdb hello-exo2.py 50`: 

```
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources-solution/hello-exo2.py(1)<module>()
-> import sys
(Pdb) s
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources-solution/hello-exo2.py(2)<module>()
-> import series
(Pdb) s
--Call--
> <frozen importlib._bootstrap>(986)_find_and_load()
(Pdb) s
> <frozen importlib._bootstrap>(988)_find_and_load()
(Pdb) s
--Call--
> <frozen importlib._bootstrap>(143)__init__()
(Pdb) s
> <frozen importlib._bootstrap>(144)__init__()
(Pdb) s
> <frozen importlib._bootstrap>(145)__init__()
(Pdb) print(i)
*** NameError: name 'i' is not defined
(Pdb) print(n)
*** NameError: name 'n' is not defined
(Pdb) break line, i == 10
*** The specified object 'line' is not a function or was not found along sys.path.
(Pdb
```

## Try 2 with import pdb and set_trace()

Q: Is there any significance of set_trace() ? or its position ?
```
ources$ python3 -m pdb hello-exo2.py 50 
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(1)<module>()
-> import pdb
(Pdb) l
  1  ->	import pdb
  2  	import sys
  3  	import series
  4  	
  5  	def main():
  6  	
  7  	    if (not len(sys.argv) == 2):
  8  	        print("Not enough parameters for the program {0}".format(sys.argv[0]))
  9  	        return
 10  	
 11  	    print("sys.argv is of type {0}".format(type(sys.argv)))
(Pdb) n
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(2)<module>()
-> import sys
(Pdb) l
  1  	import pdb
  2  ->	import sys
  3  	import series
  4  	
  5  	def main():
  6  	
  7  	    if (not len(sys.argv) == 2):
  8  	        print("Not enough parameters for the program {0}".format(sys.argv[0]))
  9  	        return
 10  	
 11  	    print("sys.argv is of type {0}".format(type(sys.argv)))
(Pdb) s
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(3)<module>()
-> import series
(Pdb) l
  1  	import pdb
  2  	import sys
  3  ->	import series
  4  	
  5  	def main():
  6  	
  7  	    if (not len(sys.argv) == 2):
  8  	        print("Not enough parameters for the program {0}".format(sys.argv[0]))
  9  	        return
 10  	
 11  	    print("sys.argv is of type {0}".format(type(sys.argv)))
(Pdb) s
--Call--
> <frozen importlib._bootstrap>(986)_find_and_load()
(Pdb) l
[EOF]
(Pdb) n
> <frozen importlib._bootstrap>(988)_find_and_load()
(Pdb) l
[EOF]
(Pdb) l
[EOF]
(Pdb) n
> <frozen importlib._bootstrap>(989)_find_and_load()
(Pdb) l
[EOF]
(Pdb) l
[EOF]
(Pdb) s
> <frozen importlib._bootstrap>(990)_find_and_load()
(Pdb) l
[EOF]
(Pdb) n
> <frozen importlib._bootstrap>(991)_find_and_load()
(Pdb) n
--Return--
> <frozen importlib._bootstrap>(991)_find_and_load()-><module 'seri...es/series.py'>
(Pdb) n
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(5)<module>()
-> def main():
(Pdb) l
  1  	import pdb
  2  	import sys
  3  	import series
  4  	
  5  ->	def main():
  6  	
  7  	    if (not len(sys.argv) == 2):
  8  	        print("Not enough parameters for the program {0}".format(sys.argv[0]))
  9  	        return
 10  	
 11  	    print("sys.argv is of type {0}".format(type(sys.argv)))
(Pdb) s
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(19)<module>()
-> if __name__ == '__main__':
(Pdb) s
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(20)<module>()
-> main()
(Pdb) s
--Call--
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(5)main()
-> def main():
(Pdb) s
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(7)main()
-> if (not len(sys.argv) == 2):
(Pdb) n
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(11)main()
-> print("sys.argv is of type {0}".format(type(sys.argv)))
(Pdb) n
sys.argv is of type <class 'list'>
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(12)main()
-> prog_name = sys.argv[0]
(Pdb) n
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(13)main()
-> N = int(sys.argv[1])
(Pdb) N
*** NameError: name 'N' is not defined
(Pdb) print(N)
*** NameError: name 'N' is not defined
(Pdb) l
  8  	        print("Not enough parameters for the program {0}".format(sys.argv[0]))
  9  	        return
 10  	
 11  	    print("sys.argv is of type {0}".format(type(sys.argv)))
 12  	    prog_name = sys.argv[0]
 13  ->	    N = int(sys.argv[1])
 14  	
 15  	    print("{0} says: Hello {1} {2}".format(
 16  	        prog_name, series.computeSeries(N), N*(N+1)/2))
 17  	
 18  	
(Pdb) n
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(15)main()
-> print("{0} says: Hello {1} {2}".format(
(Pdb) print(N)
50
(Pdb) n
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/hello-exo2.py(16)main()
-> prog_name, series.computeSeries(N), N*(N+1)/2))
(Pdb) s
--Call--
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/series.py(1)computeSeries()
-> def computeSeries(n):
(Pdb) n
> /data/asgupta/courses/sp-course/exercises/week1/python-hello/sources/series.py(2)computeSeries()
-> res = 0
(Pdb) l
  1  	def computeSeries(n):
  2  ->	    res = 0
  3  	    for i in range(1, n+1):
  4  	        res += i
  5  	
  6  	    return res
[EOF]
(Pdb) 
```
## Try 3 with pdb & set_trace 

Q:  what are these bunck of `<frozen importlib._bootstrap>` ? 

```
 <frozen importlib._bootstrap>(143)__init__()
(Pdb) s
> <frozen importlib._bootstrap>(144)__init__()
(Pdb) s
> <frozen importlib._bootstrap>(145)__init__()
(Pdb) s
--Return--
> <frozen importlib._bootstrap>(145)__init__()->None
(Pdb) s
--Call--
> <frozen importlib._bootstrap>(147)__enter__()
(Pdb) s
> <frozen importlib._bootstrap>(148)__enter__()
(Pdb) s
--Call--
> <frozen importlib._bootstrap>(157)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(163)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(164)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(165)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(166)_get_module_lock()
(Pdb) s
KeyError: 'series'
> <frozen importlib._bootstrap>(166)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(167)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(168)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(170)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(171)_get_module_lock()
(Pdb) s
> <frozen importlib._bootstrap>(174)_get_module_lock()
(Pdb) s
--Call--
> <frozen importlib._bootstrap>(58)__init__()
(Pdb) s
> <frozen importlib._bootstrap>(59)__init__()
(Pdb) n
> <frozen importlib._bootstrap>(60)__init__()
(Pdb) n
> <frozen importlib._bootstrap>(61)__init__()
(Pdb) m
*** NameError: name 'm' is not defined
(Pdb) n
> <frozen importlib._bootstrap>(62)__init__()
(Pdb) n
> <frozen importlib._bootstrap>(63)__init__()
(Pdb) n
> <frozen importlib._bootstrap>(64)__init__()
(Pdb) n
--Return--
> <frozen importlib._bootstrap>(64)__init__()->None
(Pdb) 
> <frozen importlib._bootstrap>(176)_get_module_lock()
(Pdb) 
> <frozen importlib._bootstrap>(187)_get_module_lock()
(Pdb) 
> <frozen importlib._bootstrap>(189)_get_module_lock()
(Pdb) 
> <frozen importlib._bootstrap>(191)_get_module_lock()
(Pdb) 
--Return--
> <frozen importlib._bootstrap>(191)_get_module_lock()->_ModuleLock('...40585644763552
(Pdb) 
> <frozen importlib._bootstrap>(149)__enter__()
(Pdb) 
--Return--
> <frozen importlib._bootstrap>(149)__enter__()->None
(Pdb) 
> <frozen importlib._bootstrap>(989)_find_and_load()
(Pdb) 
> <frozen importlib._bootstrap>(990)_find_and_load()
(Pdb) 
> <frozen importlib._bootstrap>(991)_find_and_load()
```
